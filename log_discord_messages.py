"""

1. visit the discord developer dashboard and generate a URL for a bot with admin privileges (the permissions bit mask integer in the URL should be long).

2. start the discord bot service:

>>> python log_discord_messages.py
{...}
We have logged in as daotest#7533

3. Visit the discord server you generated the URL for and you should see your bot among the list of onine users.
4. In the online users list on discord right click the bot and invite them to the server.
5. In discord say "hello what's up?", lowercase hello is required, (on any public channel)?

You should see something like this logged in the terminal where the bot is running.
This is the message the bot posted on the server (because that's the only print statement in the on_message fun)
This is **NOT** the original message from the user:

Received message from bot (daotest#7533):
    Hello! You just said: hello what's up?
message_obj:
<Message
    id=1009925779129389138
    channel=<TextChannel
        id=992807246788567134
        name='general'
        position=4
        nsfw=False
        category_id=992807246788567133
        news=False>
    type=<MessageType.default: 0>
    author=<Member
        id=1007105836092497950
        name='daotest'
        discriminator='7533'
        bot=True
        nick=None
        guild=<Guild
            id=992807246113276064
            name='qary'
            shard_id=0
            chunked=False
            member_count=7
            >
        >
    flags=<MessageFlags value=0>
>


"""
import datetime
import discord
import re
from discord.ext import commands
from supabase import create_client
import config

from discord import Emoji
from loggers import log, log_error, log_debug, log_warning
from utils import isoformat, to_dict, to_listodicts
from coro_utils import get_coroutine


from discord_schema import AUTHOR_ATTRS, REACTION_ATTRS, USER_ATTRS, REACTION_COROS

SUPA = create_client(config.SUPABASE_URL, config.SUPABASE_KEY)

intents = discord.Intents.default()
intents.message_content = True


DEFAULT_MESSAGE_ACTIONS = (
    # TODO: change these default args to None, and then allow the 3-tuple to be a 1-tuple and then just a str (without messing it up)
    ('record_message', (), {}),
    # ('add_reaction', ('✔',), dict(condition=dict(regex=f'\\b{BOTNAME}\\b'))),
    # ('add_reaction', ('✍️',), {}),  # 🛢  💾  📓  📄  📒  🏷   ✔  ✅  ✴  ✳  🔹  ◼  ▫
    # ('reply', ['noted'], {}),
    # TODO allow format string templates with `def render_fstring(fstring, message):`
    # ('channel.send', ['noted'], {}),
)

DEFAULT_REACTION_ACTIONS = (
    ('record_reaction', (), {}),
    # ('add_reaction', ['✔'], {}),  # ✍️  🛢  💾  📓  📄  📒  🏷   ✔  ✅  ✴  ✳  🔹  ◼  ▫
    # ('reply', ['noted'], {}),
    # TODO allow format string templates with `def render_fstring(fstring, message):`
    # ('channel.send', ['noted'], {}),
)

# Python function names that user's can specify as actions when new messages, reactions, or users are received from discord
ALLOWED_FUNS = 'record_reaction record_message insert_supabase add_reaction reply channel.send'.split()

# client = discord.Client(intents=intents)
client = commands.Bot(command_prefix=".", intents=intents)


@client.event
async def on_ready():
    print(f'Bot named {client.user} is listening for discord messages...')


def get_fun_args_kwargs(fun_name, *args, **kwargs):
    if fun_name in ALLOWED_FUNS:
        # TODO: Probably not thread safe to make ALLOWED_FUNS a subset of the dict globals()
        fun = globals().get(fun_name)
    else:
        fun = log_error
        fun_name = ''.join([c for i, c in enumerate(str(fun_name)) if i < 256])
        err_message = f"ERROR (SECURITY): {fun_name} is not in log_discord_messages.ALLOWED_FUNS!!!"
        args = [err_message] + list(args)
    return fun, args, kwargs


def update_or_create(table_name='discord_message', record_dict=None):
    log_debug(f"Attempting update_or_create(record_dict={record_dict})")
    assert record_dict is not None
    record_id = record_dict.pop('id', None)
    if record_id is not None:
        log_debug(f"Updating exisitng DB for {table_name}.id: {record_id}")
        resp = SUPA.table(table_name).select("*").eq('id', record_id).execute()
        if len(resp.data):
            return SUPA.table(table_name).update(record_dict).eq("id", record_id).execute()
        record_dict['id'] = record_id
    log_debug(f"Creating new record for {table_name}: {record_dict}!")
    return SUPA.table(table_name).insert(record_dict).execute()


def record_message(message=None):
    log_debug(f"Attempting to record message: {message}\n")
    if message is None:
        return log_error(text='Empty message_dict found in record_message()')

    message_dict = dict(
        id=message.id,  # record_id is printed on reaction.message? rather than id?
        created_at=isoformat(message.created_at),
        saved_at=isoformat(datetime.datetime.now()),
        # TODO: Throws object of type json is not serializable
        # edited_at=isoformat(message.edited_at),
        content=str(message.content),
        jump_url=str(message.jump_url),
        author=to_dict(message.author, AUTHOR_ATTRS),  # , indent=json_indent),
        reactions=to_listodicts(message.reactions, REACTION_ATTRS),  # , indent=json_indent)
    )
    return update_or_create(table_name='discord_message', record_dict=message_dict)


def record_user(user=None):
    log_debug(f"Attempting to record user: {user}\n")
    user_dict = to_dict(user, USER_ATTRS)
    log_warning(f"Attempting to record user_dict: {user_dict}\n")
    if user is None:
        return log_error(text='Empty user obj found in record_user()')
    return update_or_create(table_name='discord_user', record_dict=user_dict)


def record_reaction(reaction=None, user=None):
    """ record an individual reaction as well as the total reactions dict for all messages"""
    log_debug(f"Attempting to record reaction:\n{reaction}\nfor user:\n{user}\n")
    reaction_dict = {}
    # TODO: instead of reaction.message.reactions, use our own message.previous_reactions
    #       Our supabase message.previous reactions listofdicts contains the user for each reaction instead of just count

    if reaction is None:
        return log(text='Empty reaction or user found in record_reaction()')

    # reaction.emoji = <Emoji id = 1001468410418434200 name = 'tweet' animated = False managed = False >
    # Custom emojis: reaction.emoji = Emoji object
    # Regular emojis: reaction.emoji = String
    # Checks for emoji type and builds the reaction_dict for custom objects
    if isinstance(reaction.emoji, Emoji):
        reaction_dict = {
            'emoji': reaction.emoji.name,
            'count': reaction.count,
            'me': reaction.me,
            'message__id': reaction.message.id
        }
    else:
        reaction_dict = to_dict(reaction, REACTION_ATTRS)

    reaction_dict['user'] = user.id
    reaction_dict['discord_message'] = reaction.message.id

    resp = SUPA.table('discord_message').select("*").eq('id', reaction.message.id).execute()

    previous_reactions = resp.data[0]['reactions']

    current_reaction = {
        "me": reaction_dict['me'],
        "user": {
            "id": user.id,
            "name": user.name,
            "discriminator": user.discriminator
        },
        "count": reaction_dict['count'],
        "emoji": reaction_dict['emoji']
    }

    all_reactions = []

    if previous_reactions == []:
        all_reactions.append(current_reaction)
    else:
        previous_reactions.append(current_reaction)
        all_reactions = previous_reactions

    all_reactions = [reaction for reaction in all_reactions if not reaction['user']['id'] == config.DISCORD_BOT_ID]

    # Used for discord_reaction table only
    db_record = dict(
        message_id=reaction.message.id,
        user=user.id if user is not None else None,
        emoji=(str(reaction.emoji) or str(reaction)),
        # users=users,
    )
    db_record['previous_reactions'] = previous_reactions

    if reaction.message:
        message_dict = dict(
            id=db_record['message_id'],  # good
            created_at=isoformat(reaction.message.created_at),
            saved_at=isoformat(datetime.datetime.now()),
            content=str(reaction.message.content),
            author=to_dict(reaction.message.author, AUTHOR_ATTRS),
            reactions=all_reactions
        )

        # Original
        # record_message(reaction.message)

        update_or_create(table_name='discord_message', record_dict=message_dict)

        return update_or_create(table_name='discord_reaction', record_dict=db_record)
    return log(text=f'Empty reaction.message in record_reaction({reaction_dict})')


@ client.command()
async def add_reaction_to_top_message(message_id):
    """ Adds a flower emoji to TopMessages that are added to the GardenMessage table

    >>> await add_reaction_to_top_message(message.id)
    """
    reaction = '🌺'

    for guild in client.guilds:
        for text_channel in guild.text_channels:
            try:
                message = await text_channel.fetch_message(message_id)
            except Exception as e:
                print(e)

    return await message.add_reaction(reaction)


@ client.event
# discord.on_reaction_add(reaction, user) https://docs.pycord.dev/en/stable/api.html#discord.on_reaction_add
async def on_reaction_add(reaction, user):  # , actions=DEFAULT_REACTION_ACTIONS):  # , json_indent=2):
    actions = DEFAULT_REACTION_ACTIONS
    assert actions[0][0] == 'record_reaction'

    if reaction.message.author == client.user:
        log(f'message author ({reaction.message.author}) == client.user ({client.user})')

    responses = []
    if reaction.emoji:
        # TODO: log the entire mapping (emoji, user.id) for reaction.message in a jsonb field

        assert actions[0][0] == 'record_reaction'

        for fun_name, args, kwargs in actions:
            fun_name = str(fun_name)
            assert fun_name == 'record_reaction'  # only one action allowed, for now
            fun, args, kwargs = get_fun_args_kwargs(fun_name, *args, **kwargs)
            kwargs.update(dict(reaction=reaction, user=user))
            log(f'for fun in actions...\nfun: {fun}\nkwargs: {kwargs}')
            resp = fun(*args, **kwargs)
            print("--------------")
            log(f'fun response: {resp}')
            responses.append(resp)
    return responses


@ client.event
async def on_message(message, message_actions=DEFAULT_MESSAGE_ACTIONS):  # , json_indent=2):
    print(f'Received message from {message.author}.')
    print(f'Received message {message}.')

    # don't accidentally reply to your own messages (infinite recursion)!!!
    if message.author == client.user:
        return

    if message.channel.name == config.DISCORD_WEBHOOK_CHANNEL_NAME and message.author.name == 'bothook':
        """ Listens for a message id to be entered into the 'admin' channel of a server """
        match = re.search("(\\d+)", message.content)

        message_id = match.group()

        await add_reaction_to_top_message(message_id)

    if message.content:
        record_message(message)

        for action, args, kwargs in message_actions:
            args = tuple(args) if isinstance(args, (list, tuple)) else (args,)
            kwargs = dict(()) if kwargs is None else dict(kwargs)
            print(f'{action}(*{args}, **{kwargs})')
            if action and action.lower().startswith('insert'):
                # TODO: make this async
                log(f'Found {action}')
                # SUPA.table('discord_message').insert(message_dict).execute()
            else:
                await get_coroutine(message, action)(*args, **kwargs)

if __name__ == '__main__':
    client.run(config.BOT_TOKEN)
