#!/usr/bin/env bash
echo 'starting pycord service: python log_discord_messages.py &'
python log_discord_messages.py &
echo 'gunicorn --threads=2 stigsite.wsgi:application'
gunicorn --threads=2 stigsite.wsgi:application
