#!/usr/bin/env bash
pip install -U pip
pip install -r requirements.txt

python manage.py collectstatic --noinput
rm -rf db.sqlite3
rm -rf stigapp/migrations
# python manage.py makemigrations
python manage.py makemigrations stigapp
python manage.py makemigrations discordlogin
python manage.py migrate
sleep 1

python manage.py shell << EOF
from scripts.create_leaderboard_data import *
up = manage_leaderboard_data()
up.generate_leaderboard_data()
exit()
EOF

source .env
# echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.all().delete(); User.objects.create_superuser('$DJANGO_ADMIN_USER', '$DJANGO_ADMIN_EMAIL', '$DJANGO_ADMIN_PW')" | python manage.py shell
