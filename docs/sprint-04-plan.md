# sprint-04-plan.md

Aug 30 - Sep 5

## Sprint 04 - Tues Aug 30 - Sept 5
- [X] G8-8: Gardener users can sign into Django app with their discord single-signon (OAuth): https://www.youtube.com/watch?v=Cr-jxZ1TsuE
  * [X] G2-2: Follow tutorial
  * [X] G1-1: Fix cancel issue
  * [X] G0.5-0.5: Test tutorial
  * [X] G0.5-0.5: Integrate discord login into Daostack app
  * [X] G0.5-0.2: Change links to use reverse (0:39)
  * [X] G0.5-0.4: Rework links to make them more descriptive
  * [X] G0.5-0.2: Move credentials to .env
  * [X] G1-0.5: Clean up code for style, commenting, and ease of understanding
  * [X] G0.5-0.2: Write steps for making the discord identity connection
  * [X] G0.5-0.7: Fix formatting of stigsite homepage
  * [X] G0.5-0.1: Fix home "Login" "Logout" issue to recognize backend authentication
  * [X] Integrate Discord login with admin login
  * [X] G0.5-0.5: Implement Discord logout
  * [X] G0.5-0.1: Put profile page behind authorization wall
  * [X] G0.5-0.1: Add admin links to navbar

- [X] HG8-8: Leaderboard table view in Django admin
  * [X] G0.5 - Get updated models working locally
  * [X] GH1-1: Create Leaderboard table that's empty but viewable in admin and has a message text field
  * [X] G0.5-0.1: Python function to find first 10 messages in DiscordMessages table to populate the leader board with some message texts
  * [X] G0.5-0.15: Python function to iterate through a .all() query on the [dict(text=m.context, reaction_count=len(m.reactions) for m in DiscordMessages.objects.all()][:10]
  * [X] G05.-0.5: Use length/count above to populate top 10 messages (by longest) in LeaderBoard table with len as reaction_count field value
  * [X] G0.5-0.15: Add dynamically computed reaction_count "column" to the admin Leaderbaord view: https://realpython.com/customize-django-admin-python/
  * [X] G0.5-0.15: Update the leaderboard filtering rule/function to use the query for `reaction_count` so leaderboard is top 10 messages by reaction count
  * [X] G0.5-0.5: Test that users are being preserved now (it does not )
  * [X] G0.5-0.7: Add a column that lists each user who interacted with a message (duplicates included)
  * [X] G0.5-0.2: Add user_count column for leaderboard
  * [X] G0.5-0.1: H: Gardner would like see a string of all the emojies/reactions (`emoji_string` ) associated with a leaderboard message (started, not complete)
  * [X] G0.5-0.1- H: Gardener would like to see json `user_list` of all people that reacted to a message (not a set, but with dupes for duplicate users).  (started, not complete)

- [X] G2-1: Staging
  * [X] G0.5-0.25: Make a staging branch
  * [X] G0.5-0.25: Make a staging server on render
  * [X] G0.5-0.5: Get feature-login working on staging branch

- [X] G3-2.5: Recreate leaderboard in Supabase
  * [X] G0.5-0.7: Recreate model in Supabase
  * [X] G0.5-0.4: Add user_count, user_list, and emoji_list to Supabase model and table
  * [X] G1-1: Replicate admin view function to create data for user_count, user_list, and emoji_list in Supabase
  * [X] G0.5-0.5: Test

- [X] G0.5-0.5: Update docs
  * [X] Add info about Discord login (including ENV variables)
  * [X] Add info about applications and architecture

- [X] G3: Update LeaderBoard to interact with GardenMessage when added_to_garden field changes
  * G1: Add GardenMessage model to Supabase
  * [X] G: Add GardenMessage model to models.py
  * [X] G: Make GardenMessage editable form view in admin
  * [X] G: Add boolean field "added_to_garden"
  * [X] G: Make added_to_garden field in Leaderboard table editable "inline"
  * [X] G: When added_to_garden field is toggled true the message is used to create a GardenMessage record and associate the userid of the person that did the toggle of the add_to_garden field.
  (https://stackoverflow.com/questions/4716330/accessing-the-users-request-in-a-post-save-signal)
  * [X] G: Push to branch > staging > main (Get working on each server)

- [X] HG8-0.5: Editable rule form view in Django admin
  * [X] Create Rule table that's empty but viewable in admin

- [X] G0.5-0.1: Delete prints at server startup
- [X] G0.5-0.5: Add favicon.ico with DAOStack or TEC logo
- [X] G0.5: Update functions to handle when user information is missing.
- [X] G1-1: Get updates working on Render and test
- [X] HG1-.5 makemigrations stigapp in build.sh before migrate
- [X] G2-.5: Delete all redundant tables in Supabase (only tables should be discord_message discord_reaction discord_user discord_channel discord_guild)
- [X] G1-.5: Home page for stigsite.onrender.com/ with link to stigsite.onrender.com/admin/ endpoint and contact information for you as the admin (greg@tangibleai.com) and maybe a link to DAOStack's website

- [ ] delete prints and replace with logging.getLogger().warning()
- [ ] replace all logging calls with our custom supabaselog or Sentry logging
- [ ] create build_dev.sh and start_dev.sh with createsuperuser and makemigrations etc

- [ ] G-: Add jsonb field to discord_message table in Supabase containing the unicode, single character pictogram the emoji
- [ ] HG1: Invite Ronen and Shahar to Supabase
- [ ] G: Make Discord authentication system work with admin view



## Sprint 03 - Tues Aug 23 - Aug 30

- [X] H: docs and sprint plans on stigsite
- [X] H1: Greg access to discord qary server
- [X] H1: invite greg to daostack daogarden server
- [X] H: get supabase message recording working again
- [X] H: models.py in Django that duplicate supabase schemas
- [X] H1: connect supabase settings.DATABASES = {} to stigsite/app
- [X] H1: connect supabase to render managed postgresql db
- [X] H: upgrade stigsite to nonfree render VM
- [X] H: log reactions to supabase
- [ ] H: create config syntax for counting/analytics on messages & reactions
- [ ] H: count reactions as they occur
- [ ] H: filter messages that are logged to supabased based on minimum reactions threshold
- [ ] H: syntax for tagging messages based on thresholds for reactions, reaction types, etc
- [ ] H: analytics/plots/tables on messages logged, reactions counted
- [ ] HGR: Django OAuth login with discord account

- [ ] G: models.py schema for config file data that choses the tasks, args and kwargs
- [X] G: configure admin.py interface for all tables
- [X] H: django create superuser with password from .env (add to .env in BitWarden) during build.sh
- [ ] M: get bubble connected to supabase



## Sprint 02 Aug 11 - Aug 18

- [X] H1-1: hello discord bot on qary discord server
- [X] H1-1: supabase schema for holding messages
- [X] H1-1: push one discord message to supabase
- [X] H1-.5: access to env file on bitwarden and discord credentials
- [X] H1-.5: send links and access to all repos in daostack
- [X] H4-8: (failed) deploy stigdata to stigsite.onrender.com

- [X] G1-1: sign up for discord and play with it and subscribe/join servers that interest you
- [X] G4-3: write down and organize notes about what all these files are and what they are for (in docs
- [X] G4-4: go through the discord python bot examples that you can find (discord-py)
- [X] G2-1.5: modify the example discord bot from tutorial to retrieve many messages from discord history
- [X] G4-1: deploy empty django app stigsite to render stigsite.onrender.com or similar


## Sprint 01

- got a discord bot (stigdata) working with qary server `/hello` pycord+uvicorn+redis
- airtable
- notion
- discourse
