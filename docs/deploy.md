# Deployment

## Make accounts
Make an account at each of the following sites.
1) [Render](https://render.com/)
2) [Discord](https://discord.com/)
3) [Supabase](https://supabase.com/)
4) [Airtable](https://airtable.com/)

## Make an .env file
   The .env file will contains the credentials necessary to use the Discord bot, Discord authentication service, Discord webhook, and Supabase database.
   
   Throughout the following sections, update the .env file with the relevant values as you create the stigsite components.
   ```
   # Supabase database credentials
   SUPABASE_DOMAIN=
   SUPABASE_DB_HOST=
   SUPABASE_URL=
   SUPABASE_PW=
   SUPABASE_KEY=

   # Discord bot credentials
   PUBLIC_KEY=
   BOT_TOKEN=
   OATH2_CLIENT_ID=
   OATH2_CLIENT_SECRET=
   OATH2_GENERATED_URL=
   # DISCORD_BOT_ID is redundant, 
   # but we will keep it for now. 
   # Will delete this later
   DISCORD_BOT_ID= 
     
   # Discord user authentication
   DISCORD_LOGIN_AUTH_URL=
   DISCORD_LOGIN_CLIENT_ID=
   DISCORD_LOGIN_CLIENT_SECRET=

   # Discord webhook
   DISCORD_WEBHOOK=
   DISCORD_WEBHOOK_CHANNEL_NAME=

   # Airtable credentials
   AIRTABLE_API_KEY=
   AIRTABLE_BASE_ID=
   AIRTABLE_TABLE_NAME=
   ```
   
## Set up Supabase

### Create a database
1) Sign in to [Supabase ](https://supabase.com/).

2) From the home page, click **New project**.

3) Click **New organization**.

4) Enter in an organization name, such as 'TEC'.  Click **Create organization**.

5) In the additional fields that appear, add a Project name, such as "stigsite.""

6) Add a strong database password.

7) Enter this password in the .env file under "Supabase database credentials" *SUPABASE_PW*.

8) The project configuration data will display.

9) Click **Copy** in the Project API keys box.  Add this key to the .env file under "Supabase database credentials" *SUPABASE_KEY*.

10) In the "Project Configuration" section, click **Copy** next to URL.  Add the URL to the .env file under "Supabase database credentials" *SUPABASE_URL*.

11) For the *SUPABASE_DB_HOST* .env entry, copy the same URL as *SUPABASE_URL*.  Modify that copy to replace "https://" with "db."

12) For the *SUPABASE_DOMAIN* .env entry, copy the same URL as *SUPABASE_URL*.  Modify that copy by deleting. "https://"

The URLs should look similar to the following in your .env file.

```
   SUPABASE_DOMAIN=adhjkegnjkganjkajlka.supabase.co
   SUPABASE_DB_HOST=db.adhjkegnjkganjkajlka.supabase.co
   SUPABASE_URL=https://adhjkegnjkganjkajlka.supabase.co
```

### Set up Supabase tables
You need to manually make the tables below for Stigapp to work.

You can add the tables below by going to the **Table editor** section of your Supabase organization's project.

|Table Name | Description |
|---        |---          |
|auth_user | Users who have logged in with their Discord account |
|auth_user_groups | A dummy table to satisfy Django's delete function |
|auth_user_user_permissions | A dummy table to satisfy Django's delete function |
|garden_message | A collection of messages chosen by Gardener's from the **top_message** table |
|top_message | A collection of messages from **discord_message** that have met threshold conditions |
|discord_message | Messages from a Discord server collected by the log_message_data Discord Bot |
|discord_reaction | Reacts from a Discord server collection by the log_message_data Discord Bot |
|log | A record of events in log_message_data Discord Bot |

#### 1) auth_user

   | field | type | default |
   |---    |---   |---      |
   | id | int8 | PRIMARY |
   | flags | int8 | NULL |
   | public_flags | int8 | NULL |
   | avatar | varchar | NULL |
   | discord_tag | varchar | NULL |
   | email | varchar | NULL |
   | locale | varchar | NULL |
   | first_name | varchar | NULL |
   | last_name | varchar | NULL |
   | password | varchar | NULL |
   | username | varchar | NULL |
   | date_joined | timestamptz | NULL |
   | last_login | timestamptz | NULL |
   | active | bool | NULL |
   | admin | bool | NULL |
   | mfa_enabled | bool | NULL |
   | staff | bool | NULL |
   
#### 2) auth_user_groups

   | field | type | default |
   |---    |---   |---      |
   | id | int8 | PRIMARY |
   | group_id | int8 | NULL |
   | user_id | int8 | NULL |
   
#### 3) auth_user_user_permissions

   | field | type | default |
   |---    |---   |---      |
   | id | int8 | PRIMARY |
   | permission_id | int8 | NULL |
   | user_id | int8 | NULL |
   
#### 4) garden_message

   | field | type | default |
   |---    |---   |---      |
   | id | int8 | PRIMARY |
   | message_id | int8 | NULL |
   | unique_reaction_count | int8 | NULL |
   | user_count | int8 | NULL |
   | original_message | text | NULL |
   | editable_message | text | NULL |
   | user_list | varchar | NULL |
   | author | jsonb | NULL |
   | counts_by_emoji | jsonb  | NULL |
   | emoji_list | jsonb | NULL |
   | reactions | jsonb | NULL |
   | unique_emoji_list | jsonb | NULL |
   | unique_user_count | int8 | NULL |
   | unique_user_list | jsonb | NULL |
   | user_list | jsonb | NULL |
   | reaction_count | float4 | NULL |


#### 5) top_message

   | field | type | default |
   |---    |---   |---      |
   | id | int8 | PRIMARY |
   | message_id | int8 | NULL |   
   | unique_reaction_count | int8 | NULL |
   | unique_user_count | int8 | NULL |
   | user_count | int8 | NULL |
   | reaction_count | float4 | NULL |
   | original_message | text | NULL |
   | added_to_garden | bool | false |
   | author | jsonb | NULL |
   | counts_by_emoji | jsonb | NULL |
   | emoji_list | jsonb | NULL |   
   | reactions | jsonb | NULL |
   | unique_emoji_list | jsonb | NULL |
   | unique_user_list | jsonb | NULL |
   | user_list | jsonb | NULL |


#### 6) discord_message

   | field | type | default |
   |---    |---   |---      |
   | id | int8 | PRIMARY |
   | content | varchar | NULL |
   | author | jsonb | NULL |
   | reactions | jsonb | NULL |
   | created_at | timestamptz | now() |
   | edited_at | timestamptz | NULL |
   | saved_at | timestamptz| now() |

   
#### 7) log

   | field | type | default |
   |---    |---   |---      |
   | id | int8 | PRIMARY |
   | created_at | timestamptz | now() |
   | json | jsonb | NULL |
   | text | varchar | NULL |
   | log_level | int2 | NULL |
   
#### 8) discord_reaction

   | field | type | default |
   |---    |---   |---      |
   | id | int8 | PRIMARY |
   | discord_message | int8 | NULL |
   | message_id | int8 | NULL |
   | user | int8 | NULL |
   | created_at | timestamptz | now() |
   | emoji | text | NULL |
   | previous_reactions | jsonb | NULL |
   | previous_reaction_users | jsonb | NULL |
   | users | jsonb | NULL |


* **NOTE:** Django will create other tables during set up, including auth_group, auth_group_permissions, django_admin_log, django_content_type, django_migrations, and django_session.  You will need to verify that these tables are present for the application to work.

### Get a Supabase SSL certificate
You will need to include the contents of this certificate into the Render webhosting service later.

1) Click **Settings** (the gear icon).

2) Click **Database**.

3) Scroll down to the "SSL Connection" section.

4) Click **Download Certificate**.

## Set up the Discord components

### Create a Discord bot
1) Log in to the [Discord Developer Portal](https://discord.com/developers/applications) with your Discord login.

2) Click *New Application*.

3) Give the chatbot a name.  Agree to the Terms of Service.  Click *Create*.

4) Copy the APPLICATION ID.  Put it in the .env file under "Discord bot credentials" *OAUTH_CLIENT_ID* and *DISCORD_BOT_ID*.

5) Copy the *PUBLIC KEY*.  You should put this in the .env file under "Discord bot credentials" *PUBLIC_KEY*.

6) From the sidebar, click **Bot**.

7) Click **Add Bot**.  Then click, **Yes, do it!**.

8) Click **Reset Token**.

9) Click **Yes, do it!**.

10) Click **Copy**.

11) You should put this token in the .env file under "Discord bot credentials" *BOT_TOKEN*.

12) From the sidebar, click **OAuth2**.

13) Click on **Reset Secret**.

14) Click **Copy**.

15) Put the CLIENT SECRET in the .env file under "Discord bot credentials" under *OATH2_CLIENT_SECRET*.

16) Select **URL Generator**.

17) In SCOPES, select **bot**.

18) In BOT PERMISSIONS, select all the checkboxes under TEXT PERMISSIONS.

19) In GENERATED URL, click **Copy**.

20) Put the GENERATED URL in the .env file under "Discord bot credentials" *OAUTH2_GENERATED_URL*.

21) Visit the URL in your browser.

22) Select the server you want the bot to work in. Click **Continue**.  Then, click **Authorize**.



### Create a Discord identity application

1) Log in to the [Discord Developer Portal](https://discord.com/developers/applications) with your Discord login.

2) Click *New Application*.

3) Give the identity application a name, such as StigIdentity.  Agree to the Terms of Service.  Click *Create*.

4) From the sidebar, click **OAuth2**.

5) Click **Reset Secret**.

6) Click **Yes, do it!**.

7) Click **Copy**.

8) Put the CLIENT SECRET in the .env file under "Discord user authentication" *DISCORD_LOGIN_CLIENT_SECRET*.

9) Under "Client ID", click **Copy**.  Save this value in the .env file under "Discord user authentication" *DISCORD_LOGIN_CLIENT_ID*.

10) Under "Redirects", click **Add Redirect**.

11) Enter the redirect for the server you are working on.  The endpoint should be "/oauth2/login/redirect" (ie., http://localhost:8000/oauth2/login/redirect).  Click **Save Changes** when finished.

12) Select **URL Generator**.

13) In SCOPES, select **identify**.

14) In SELECT REDIRECT URL, select a redirect URL from the dropdown menu (Example: http://localhost:8000/oauth2/login/redirect).

15) Under GENERATED URL, click **Copy**.

16) Save the generated url in the .env as *DISCORD_LOGIN_AUTH_URL*.

### Set up a Discord webook and private admin channel

1) In the Discord server the bot will listen to, create a private text channel.
   a. Click the + icon.
   b. Give the channel the name 'admin'.
   c. For Private Channel, select True (or on).
   d. Click **Create Channel**.

2) Select the channel you just created.

3) Click the Edit Channel **gear icon**.

4) Click **Integrations**.

5) Click **Create Webhook**.

6) Name the webhook "bothook".

7) Select the channel you created earlier (ie., "admin").

8) Click **Save Changes**.

9) Click **Copy Webhook URL**.

10) Save the webhook URL in the .env under "Discord webhook"  *DISCORD_WEBHOOK*.
   
11) Save the channel name you set (ie., "admin") in the .env under "Discord webhook"  *DISCORD_WEBHOOK_CHANNEL_NAME*.   

## Set up Airtable integration
This integration is not complete at the moment.  However, a test script references these values.  You can either use dummy values or set up a an Airtable.

1) Log in to [Airtable](https://airtable.com/).

2) Click the profile image.

3) Click **Account**.

4) Under the API section, click **Generate API key**.

5) Copy the API key.

6) In the .env file under "Airtable credentials", add the API key to *AIRTABLE_API_KEY*.

7) Go back to your Airtable home page.  Create an Airtable base.

8) The base will have an Airtable similar to the following:
https://airtable.com/appbDrod2abrajcNS/tblsdJYFSJclFAHAA/viwDjfshEw4gHDhDUq?blocks=hide

9) Copy the portion of the URL that starts with "app" (ie., "appbDrod2abrajcNS").  Add this to the *AIRTABLE_BASE_ID*.

10) You need to specify the name of the table in the base you want the API to work with.  The table name is in the tab above the table.  Add this to the *AIRTABLE_TABLE_NAME*.


## Set up Render web service
In order to follow these steps, you will need to enter credit card information for the account because we will be using the Blueprint feature.

Using the Blueprint feature, you will be able to spin up new instances of the application with similar settings based on a common Git*** repository.

However, you will still be able to define the features unique to each project, such as .env credentials from above.

1) Log in to [Render](https://render.com/).

2) Click **Blueprints**.

3) Click **New Blueprint Instance**.

4) Enter into the **Public Git repository** field the repository you will be using (ie.,  https://gitlab.com/tangibleai/stigsite).  Click **Continue**.

* **NOTE:** The current application requires hardcoding values for things like approved admin users.  You will need to adjust the constants.py file or the discordlogin/models.py users to your project's needs.

5) For the Service Group Name, enter a project name, such as stigsite.

6) Click **Create New Resources**.

7) Click **Apply**.

8) Render will start to build the application.  However, the application will fail because we need to set up the .env files and build/start commands.  Click on **Dashboard**.

9) Click on the new application.

10) Click on **Environment**.

11) Click **Add Secret File**.

12) For Filename, write .env.  For File contents, copy the contents of the .env you have built throughout this document.

13) Click **Add Secret File** again.

14) For Filename, write secrets_prod-ca-2021.crt.  Copy the contents of that file.

15) Click **Save Changes**.

16) Click **Events**.  

17) Click **Deploy** in the most current deployment.  This is evident by three dots undulating.

18) When the application successfully deploys, Render will display the word "live" in a green box.  The terminal will display the following:
```
Sep 20 05:16:04 PM  ==> Uploading build...
Sep 20 05:16:31 PM  ==> Build uploaded in 12s
Sep 20 05:16:31 PM  ==> Build successful 🎉
Sep 20 05:16:31 PM  ==> Deploying...
Sep 20 05:17:33 PM  ==> Starting service with 'source ./start.sh'
Sep 20 05:17:33 PM  starting pycord service: python log_discord_messages.py &
Sep 20 05:17:33 PM  gunicorn --threads=2 stigsite.wsgi:application
Sep 20 05:17:38 PM  2022-09-20 08:17:38,019:WARNING - PyNaCl is not installed, voice will NOT be supported
Sep 20 05:17:38 PM  2022-09-20 08:17:38,021:INFO - logging in using static token
Sep 20 05:17:38 PM  2022-09-20 08:17:38,525:INFO - Shard ID None has sent the IDENTIFY payload.
Sep 20 05:17:38 PM  [2022-09-20 08:17:38 +0000] [55] [INFO] Starting gunicorn 20.1.0
Sep 20 05:17:38 PM  [2022-09-20 08:17:38 +0000] [55] [INFO] Listening at: http://0.0.0.0:10000 (55)
Sep 20 05:17:38 PM  [2022-09-20 08:17:38 +0000] [55] [INFO] Using worker: gthread
Sep 20 05:17:38 PM  [2022-09-20 08:17:38 +0000] [58] [INFO] Booting worker with pid: 58
Sep 20 05:17:38 PM  [2022-09-20 08:17:38 +0000] [60] [INFO] Booting worker with pid: 60
```
