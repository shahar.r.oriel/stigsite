# Sprint-07-plan

- [ ] H: invite daotest bot to TEC channel that Gideon wants this week
- [x] G8-5: bot can detect and store the custom server-specified reactions like the TEC logo (all reactions listed when you add a reaction yourself), log_discord_message pycord
- [x] G0.5-0.1: parse the message id to create url for link to original message in discord
- [x] G0.5-0.1: hardcode a new rule to create messages in TopMessage table whenever the number of reaction unique users exceeds 5
- [x] G0.5-0.5: switch stigsite logo to CommonSenseMakers logo on home page
- [x] G: Only allow admin users to see home page tables (entire app?)
- [x] G0.5-0.5: Parse Author json to contain name#1234 named "AuthorDiscordUserName" fully qualified username
- [x] G0.5-0.1: Swap column names in view of table for GardenMessages and TopMessages so they only see "Author" column and it contains human-readable discord username
- [ ] S: check exported GardenMessage table imported into Airtable
- [x] G0.5-0.1: Force export csv of garden message such that it looks in Excel just like it does on the home page
- [x] G0.5-0.1: Force export csv of top message such that it looks in Excel just like it does on the home page
- [x] G0.5-0.1: Add editable text field to GardenMessage table named "twitter handle" and populated with an empty string for new garden messages adds
- [x] G0.5-0.1: low priority: put editable fields first in admin views of TopMessage Table and GardenMessage table (Put it after the id to avoid complication with linking to full data)
- [ ] G: Add field to garden message table contain comma or newline-separated **tags** similar to the unique emojies string field currently in the table
- [x] G2-2: Update Databases
  - [x] Add database field with timestamp of the original authorship of message
        * [x] DiscordMessage - **exists**
        * [x] TopMessage - **added**
        * [x] GardenMessage - **added**
  - [x] Add database field with timestamp of the latest discord message edit
        * [x] DiscordMessage - **exists**
        * [x] TopMessage - **unnecessary, because can't edit in this table**
        * [x] GardenMessage - X
  - [x] Add database field with timestamp of the last export of the csv it should contain the same exact time stamp for all messages in a given export
        * [x] TopMessage - **added**
        * [x] GardenMessage - **added**
- [x] G1-1: Add database export_timestamp (if required) field with timestamp of the last export of the csv it should contain the same exact time stamp for all messages in a given export
- [ ] G: Every 3 minutes run the update top message table with new messages


## Backlog

- [ ] Add database table mapping discord user to twitter user
- [ ] Add twitter handle database field to garden message of the discord author

- [x] Bug: Rules table is broken in admin interface
- [ ] Add unique users > 5 rule to rules table as data so that admin user can change threshold value
- [ ] add field gardenmessage table for message type: [thread reply, or inthread post, or normal discord message]
- [ ] Server(Guild) + Channel pair as field for GardenMessage table
- [ ] Add list of discord user names filter for general access 
- [ ] Add filter on auth for a server

## Notes
- [ ] forum feature creates threads for each message and message id shared with thread id
but
