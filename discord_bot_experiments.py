""" Some ideas for a bot that can react to and block messages to help with content curation

If it were configurable most of this code would be good for the stigbot (DAOStack stigsite for Stigliani-style sense-making)
"""
import asyncio
import discord
import json
import os
import pandas as pd
import random
import requests

from dotenv import load_dotenv
from supabase import create_client, Client


load_dotenv()

url = os.getenv('SUPABASE_URL')
key = os.getenv('SUPABASE_KEY')
supabase: Client = create_client(url, key)


# Need to make sure that the bot has message_content intent activated (Aug 31)
# Need to import all intents


""" Default intents
Doesn't get the message.contents intent
intents = discord.Intents.default()

Gets the message.contents intent
intents = discord.Intents.all()
"""


client = discord.Client(intents=discord.Intents.all())
token = os.environ.get('DISCORD_CLIENT')

# block_words = ["dog", "bird", "fish", "https://"]
# blacklist of words (case insensitive) that will be blocked/deleted by the bot
block_words = ["dog", "bird", "fish"]


def retrieve_messages(channelid):
    """ Uses the channel id to get messages through a request """
    headers = {
        'authorization': os.environ.get('DISCORD_AUTHORIZATION')
    }
    r = requests.get(f'https://discord.com/api/v9/channels/{channelid}/messages', headers=headers)

    response = json.loads(r.text)
    for value in response:
        print(value, '\n')


@client.event
async def on_ready():
    print(f'Bot logged in as {client.user}')


@client.event
async def on_message(message):
    """ Sets up what happens when the Discord server receives a message """

    # See information about the post/poster
    username = str(message.author).split('#')[0]
    user_message = str(message.content)
    channel = str(message.channel.name)
    print(f'{username}: {user_message} ({channel})')

    # The bot will not listen to its own messages
    if message.author == client.user:
        return

    # The bot will only respond in the specified channel
    if message.channel.name == 'general':
        """ A set of basic commands
        hello - Sends a greeting to the user
        bye - Sends a recognition to the user
        !random - Sends a message with a random number
        """

        for text in block_words:
            """ Deletes a message that contains words in the blocked_words list """
            if text in str(message.content.lower()):
                await message.delete()
                return

        if user_message.lower() == 'hello':
            await message.channel.send(f'Hello {username}!')
            return
        elif user_message.lower() == 'bye':
            await message.channel.send(f'See you later, {username}')
            return
        elif user_message.lower() == '!random':
            response = f'This is your random number: {random.randrange(100)}'
            await message.channel.send(response)
            await message.channel.send(f'See you later, {username}!')
            return

    if message.content.startswith('*'):
        """ A set of commands to test adding reactions.
        *happy - Adds a cat reaction to the user's message
        *sad - Adds a randomly select emoji to the user's message
        *code - Adds an emoji to the user's message using unicode value
        """
        cmd = message.content.split()[0].replace("*", "")
        print(cmd)

        if cmd == 'happy':
            await message.add_reaction("😼")

        sad_emojis = ['☹️', '😭', '😢', '😫']
        if cmd == 'sad':
            await message.add_reaction(random.sample(sad_emojis, 1)[0])

        if cmd == 'code':
            await message.add_reaction('\u2705')
            # http://xahlee.info/comp/unicode_emoticons.html
            # await message.add_reaction('\u1F92E')

    if message.content.startswith('_'):
        """ A set of commands to test extracting message history
        _scan - Retrieves messages from the chat history and creates an CSV with them
        _history - Retrieve messages from a chatroom using the channel id
        _messages - Retrieves the full list of messages and sends them to Supabase
        """
        cmd = message.content.split()[0].replace("_", "")
        await message.channel.send(cmd)

        if len(message.content.split()) > 1:
            parameters = message.content.split()[1:]

        # Gets a list of messages, but only got 5
        if cmd == 'scan':
            data = pd.DataFrame(columns=['content', 'time', 'author'])

            def is_command(msg):
                if len(msg.content) == 0:
                    return False
                elif msg.content.split()[0] == '_scan':
                    return True
                else:
                    return False

            async for msg in message.channel.history(limit=10):
                if msg.author != client.user:
                    if not is_command(msg):
                        data = data.append({
                            'content': msg.content,
                            'time': msg.created_at,
                            'author': msg.author.name
                        }, ignore_index=True)
                    if len(data) == 10:
                        break
                file_location = "data.csv"
                data.to_csv(file_location)

        """ Uses Requests through retrieve_messages function to get a full object for each message """
        if cmd == 'history':
            retrieve_messages(os.getenv('SERVER_ID'))

        """ Get the full list of messages in the message.channel.history
        Returns a custom object that contains content and author for each message """
        if cmd == 'messages':
            # messages = await message.channel.history(limit=500).flatten()
            data = []
            async for msg in message.channel.history(limit=500):
                data.append({
                    'content': msg.content,
                    # 'time': msg.created_at,
                    'author': msg.author.name
                })
            print(data)
            supabase.table(os.getenv('SUPABASE_TABLE')).insert(data).execute()

    if user_message.lower() == '!anywhere':
        await message.channel.send('This can be used anywhere!')
        return


client.run(token)

""" Good Tutorials
- https: // www.youtube.com / watch?v = fU - kWx - OYvE
- https: // www.youtube.com / watch?v = Q8wxin72h50
- https: // www.youtube.com / watch?v = xh28F6f - Cds
"""
