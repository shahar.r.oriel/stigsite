import os
import random

from pyairtable import Table
from dotenv import load_dotenv
load_dotenv()

# AIRTABLE_API_KEY = os.environ.get('AIRTABLE_API_KEY')
# AIRTABLE_BASE_ID = os.environ.get('AIRTABLE_BASE_ID')
# AIRTABLE_TABLE_NAME = os.environ.get('AIRTABLE_TABLE_NAME')

# print(f"Key: {AIRTABLE_API_KEY}, BASE_ID: {AIRTABLE_BASE_ID}, TABLE_NAME: {AIRTABLE_TABLE_NAME}")

# table = Table(AIRTABLE_API_KEY, AIRTABLE_BASE_ID, AIRTABLE_TABLE_NAME)

# # Retrieves all the data in the table
# print(table.all())

# # Adds a row to the table
# # table.create({'Name': 'Test3'})

# # Updates a specific row with the information provided
# # table.update('recL6RmWPVjyQwL82', {'Name': 'Updated again!'})

# # Will automatically update ranking when the new data is added be filter is applied to the Base

# # No good way to add arrays as a cell value
# # https://community.airtable.com/t/add-an-array-of-elements-in-single-column-cell/23220/3
# dummy_emojis_img = ['🇰🇷','🐮','👻','👻','👻','🥞','🍉','🍉','😆','😼','😼','😼','😼','📢','🚀','🧀','🐣','🧠']
# unique_emojis = set(dummy_emojis_img.copy())

# # emoji_counts = ''.join(dummy_emojis_img)

# emoji_counts = {}
# for emoji in dummy_emojis_img:
#     emoji_counts[emoji] = emoji_counts.get(emoji, 0) + 1

# emoji_counts_str = str(emoji_counts)

# print("Unique Emojis")
# print(unique_emojis)
# print("Emoji Counts")
# print(emoji_counts_str)
# print(type(emoji_counts_str))


# for num in range(10):
#     num = random.randint(0,50)
#     table.create({
#         "Message": f"Message {num}",
#         "Emojis": num,
#         "Emoji List": str(unique_emojis),
#         "Emoji Counts": str(emoji_counts),
#         "👻": emoji_counts['👻'],
#         "😆": emoji_counts['😆']
#     })


class AirTableService:
    """ Adds data to the DAO leaderboard on Airtable
    """

    def __init__(self):
        self._api_key = os.environ.get('AIRTABLE_API_KEY')
        self._airtable_base_id = os.environ.get('AIRTABLE_BASE_ID')
        self._airtable_table_name = os.environ.get('AIRTABLE_TABLE_NAME')

    def update_leaderboard(self):
        table = Table(self._api_key, self._airtable_base_id, self._airtable_table_name)

        dummy_emojis_img = ['🇰🇷', '🐮', '👻', '👻', '👻', '🥞', '🍉', '🍉', '😆', '😼', '😼', '😼', '😼', '📢', '🚀', '🧀', '🐣', '🧠']
        unique_emojis = set(dummy_emojis_img.copy())

        emoji_counts = {}
        for emoji in dummy_emojis_img:
            emoji_counts[emoji] = emoji_counts.get(emoji, 0) + 1

            emoji_counts_str = str(emoji_counts)

        for num in range(10):
            num = random.randint(0, 50)
            table.create({
                "Message": f"Message {num}",
                "Emojis": num,
                "Emoji List": str(unique_emojis),
                "Emoji Counts": str(emoji_counts),
                "👻": emoji_counts['👻'],
                "😆": emoji_counts['😆']
            })
