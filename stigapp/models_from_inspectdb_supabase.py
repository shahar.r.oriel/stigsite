""" models.py auto-generated using the inspectdb django manage command

WARNING: This file may be corrupted by print statements in settings.py

>>> python manage.py inspectdb --database supabase >> stigapp/models_from_inspectdb.py

"""

from django.db import models


class DiscordMessage(models.Model):
    id = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True)
    edited_at = models.DateTimeField(blank=True, null=True)
    content = models.CharField(max_length=-1, blank=True, null=True)
    author = models.JSONField(blank=True, null=True)
    reactions = models.JSONField(blank=True, null=True)
    saved_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'discord_message'


class Log(models.Model):
    id = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True)
    json = models.JSONField(blank=True, null=True)
    text = models.CharField(max_length=-1, blank=True, null=True)
    log_level = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'log'
