import csv
import datetime
import json

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.template.defaultfilters import register
from django.urls import reverse
from django.utils import timezone
from django.views.generic import ListView

from .models import TopMessage
from scripts.create_leaderboard_data import *
from stigapp.models import (
    DiscordMessage,
    DiscordReaction,
    GardenMessage
)


@register.filter(name='dict_key')
def dict_key(d, k):
    return d[k]


def home(request):
    return render(request, 'stigapp/home.html')


def about(request):
    return render(request, 'stigapp/about.html')


@login_required(login_url='/login')
def profile(request):
    return render(request, 'stigapp/profile.html')


@login_required(login_url='/login')
def update_top_message_list(request):

    up = manage_leaderboard_data()
    up.generate_leaderboard_data()

    return HttpResponseRedirect(reverse('garden_messages'))


def extract_reaction_data(entry):
    """ Converts reaction and user data from JSON to strings for the TopMessage and Garden Message CSV export functions

    Input:
    GardenMessage object (29)

    Output:
    {
        'emoji_list': '😭, 🥵, 🪂, 🛤️, 😭, 🥵, 🪂, 🛤️',
        'unique_emoji_list': '🛤️, 🪂, 😭, 🥵',
        'user_list': 'thompsgj8407, thompsgj8407, thompsgj8407, thompsgj8407, gregt3302, gregt3302, gregt3302, gregt3302', 'unique_user_list': 'thompsgj8407, gregt3302'
    }

    """

    emoji_list = []
    user_list = []

    for reaction in entry.reactions:
        emoji_list.append(reaction['emoji'])
        user_list.append(f"{reaction['user']['name']}{reaction['user']['discriminator']}")

    reaction_data_dict = {
        "emoji_list": ", ".join(emoji_list),
        "unique_emoji_list": ", ".join(list(set(emoji_list))),
        "user_list": ", ".join(user_list),
        "unique_user_list": ", ".join(list(set(user_list))),
        "counts_by_emoji": "\n".join("{}: {}".format(k, v) for k, v in entry.counts_by_emoji.items())
    }

    return reaction_data_dict


@login_required(login_url='/login')
def export_top_message_csv(request):
    """ Exports a CSV of all the data in the TopMessage table """
    leaderboard_data = TopMessage.objects.all()
    filename = f"TopMessage-{datetime.date.today()}.csv"
    response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename=' + filename}
    )

    response.write(u'\ufeff'.encode('utf8'))
    writer = csv.writer(response)
    writer.writerow([
        'Author',
        'Message ID',
        'Message',

        'All Reactions',
        'Unique Reactions',

        'All Emojis',
        'Unique Emojis',

        'Totals by Emoji',

        'Total Users',
        'Unique Users',

        'Total User List',
        'Unique User List'

        'Added to Garden',
        "URL"
    ])

    export_time = timezone.now()

    for entry in leaderboard_data:
        reaction_data_dict = extract_reaction_data(entry)
        writer.writerow([
            entry.author_discord_username,
            entry.message_id,
            entry.original_message,

            entry.reaction_count,
            entry.unique_reaction_count,

            reaction_data_dict['emoji_list'],
            reaction_data_dict['unique_emoji_list'],

            reaction_data_dict['counts_by_emoji'],

            entry.user_count,
            entry.unique_user_count,

            reaction_data_dict['user_list'],
            reaction_data_dict['unique_user_list'],

            entry.added_to_garden,
            entry.jump_url
        ])

        TopMessage.objects.filter(
            message_id=entry.message_id
        ).update(
            exported_at=export_time
        )

    return response

# TODO: Can these two exports be combined into one?


@login_required(login_url='/login')
def export_garden_message_csv(request):
    """ Exports a CSV of all the data in the GardenMessage table """
    leaderboard_data = GardenMessage.objects.all()
    filename = f"GardenMessage-{datetime.date.today()}.csv"
    response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename=' + filename}
    )

    response.write(u'\ufeff'.encode('utf8'))
    writer = csv.writer(response)
    writer.writerow([
        'Author',
        'Twitter Handle',

        'Message ID',
        'Original Message',
        'Editable Message',

        'All Reactions',
        'Unique Reactions',

        'All Emojis',
        'Unique Emojis',

        'Totals by Emoji',

        'Total Users',
        'Unique Users',

        'Total User List',
        'Unique User List',

        'URL'
    ])

    export_time = timezone.now()

    for entry in leaderboard_data:

        reaction_data_dict = extract_reaction_data(entry)

        writer.writerow([
            entry.author_discord_username,
            entry.twitter_handle,

            entry.message_id,
            entry.original_message,
            entry.editable_message,

            entry.reaction_count,
            entry.unique_reaction_count,

            reaction_data_dict['emoji_list'],
            reaction_data_dict['unique_emoji_list'],

            reaction_data_dict['counts_by_emoji'],

            entry.user_count,
            entry.unique_user_count,

            reaction_data_dict['user_list'],
            reaction_data_dict['unique_user_list'],

            entry.jump_url
        ])

        GardenMessage.objects.filter(
            message_id=entry.message_id
        ).update(
            exported_at=export_time
        )

    return response


# class LeaderBoardView(ListView):
#     model = LeaderBoard
#     ordering = ['-reaction_count']

class GardenMessageListView(ListView):
    """ A view that uses gardenmessage_list.html to display an html table for both GardenMessage and TopMessage Supabase tables

    Accesed in the template with gardenmessage_list"""
    model = GardenMessage
    ordering = ['-reaction_count']

    def get_context_data(self, **kwargs):
        """ Puts the TopMessage table into a context variable because this ListView only contains GardenMessage by default

        Accessed in the template with topmessage_list"""
        context = super(GardenMessageListView, self).get_context_data(**kwargs)
        context['topmessage_list'] = TopMessage.objects.all().order_by('-reaction_count')
        return context


class DiscordMessageListView(ListView):
    model = DiscordMessage


class DiscordReactionListView(ListView):
    model = DiscordReaction
    ordering = ['-previous_reactions']
