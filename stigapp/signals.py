import config

from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from discord import SyncWebhook
# from .airtable_integration import AirTableService
from .models import GardenMessage, TopMessage


# @receiver(post_save, sender=TopMessage)
# def signal_update_airtable(sender, instance, created, **kwargs):
#     air_class = AirTableService()
#     air_class.update_leaderboard()


def create_or_update_garden_message(instance):
    """ Copies the current information about a message from the TopMessage table to the GardenMessage table """
    gm, created = GardenMessage.objects.get_or_create(
        user_id='test',
        author=instance.author,
        author_discord_username=instance.author_discord_username,
        message_id=instance.message_id,
        created_at=instance.created_at,
        original_message=instance.original_message,
        editable_message=instance.original_message,
        reactions=instance.reactions,
        reaction_count=instance.reaction_count,
        emoji_list=instance.emoji_list,
        user_count=instance.user_count,
        user_list=instance.user_list,
        unique_reaction_count=instance.unique_reaction_count,
        unique_emoji_list=instance.unique_emoji_list,
        unique_user_count=instance.unique_user_count,
        unique_user_list=instance.unique_user_list,
        counts_by_emoji=instance.counts_by_emoji,
        jump_url=instance.jump_url
    )

    if created:
        print(f"GardenMessage created: {gm}")
    else:
        # TODO: Need to think about which fields should be "instance" (ie., TopMessage)
        GardenMessage.objects.filter(message_id=gm.message_id).update(
            user_id='UPDATED!!!!!!',
            author=gm.author,
            author_discord_username=gm.author_discord_username,
            message_id=gm.message_id,
            created_at=gm.created_at,
            original_message=gm.original_message,
            editable_message=gm.editable_message,
            reactions=instance.reactions,
            reaction_count=instance.reaction_count,
            emoji_list=instance.emoji_list,
            user_count=instance.user_count,
            user_list=instance.user_list,
            unique_reaction_count=instance.unique_reaction_count,
            unique_emoji_list=instance.unique_emoji_list,
            unique_user_count=instance.unique_user_count,
            unique_user_list=instance.unique_user_list,
            counts_by_emoji=instance.counts_by_emoji,
            jump_url=gm.jump_url
        )
        print(f"GardenMessage updated: {gm}")


def trigger_reward_emoji(message_id):
    """ Sends a message_id to the admin channel to indicate the Discord bot should add a flower emoji to the message """
    webhook = SyncWebhook.from_url(
        config.DISCORD_WEBHOOK)
    webhook.send(f"Message {message_id} was sent to the Greenhouse!")


@receiver(post_save, sender=TopMessage)
def top_message_signal(sender, instance, created, **kwargs):
    """ Triggers when a TopMessage row is saved.

    If added_to_garden field is true, adds the message to the GardenMessage table and triggers a reward emoji for the message
    """

    if instance.added_to_garden is True:
        create_or_update_garden_message(instance)

        trigger_reward_emoji(instance.message_id)
