from django.contrib import admin
# from stigapp.models import Author, Reaction, Message, Score
from stigapp.models import (
    DiscordMessage,
    DiscordReaction,
    GardenMessage,
    Log,
    Rule,
    TopMessage
)


class DiscordMessageAdmin(admin.ModelAdmin):
    model = DiscordMessage
    list_display = [field.name for field in DiscordMessage._meta.get_fields()]


class DiscordReactionAdmin(admin.ModelAdmin):
    model = DiscordReaction
    list_display = [field.name for field in DiscordReaction._meta.get_fields()]


class GardenMessageAdmin(admin.ModelAdmin):
    model = GardenMessage
    list_display = (
        "id",
        "editable_message",
        "edited_at",
        "twitter_handle",
        "user_id",
        "original_message",
        "reactions",
        "unique_reaction_count",
        "unique_emoji_list",
        "counts_by_emoji",
        "unique_user_count",
        "unique_user_list",
        "created_at",
        "exported_at"
    )
    list_editable = (
        "editable_message",
        "twitter_handle"
    )
    readonly_fields = ['user_id', 'original_message', 'reactions']

    def get_readonly_fields(self, request, obj=None):
        # Can distinguish between superusers and staff
        # if request.user.is_superuser:
        #     return []
        return self.readonly_fields


class LogAdmin(admin.ModelAdmin):
    model = Log
    list_display = [field.name for field in Log._meta.get_fields()]


class RuleAdmin(admin.ModelAdmin):
    list_display = (
        "rule_name",
        "chosen_reactions",
        "channel_name",
        "role",
        "threshold",
        "activated_status"
    )
    list_editable = (
        "chosen_reactions",
        "channel_name",
        "role",
        "threshold",
        "activated_status"
    )


class TopMessageAdmin(admin.ModelAdmin):
    model = TopMessage

    # list_display = [field.name for field in TopMessage._meta.get_fields()]
    list_display = [
        'id',
        'added_to_garden',
        'author_discord_username',

        'original_message',
        'reaction_count',
        'unique_reaction_count',
        'emoji_list',
        'unique_emoji_list',
        'counts_by_emoji',
        'user_count',
        'unique_user_count',
        'user_list',
        'unique_user_list',
        "created_at",
        "exported_at"
    ]

    list_editable = (
        "added_to_garden",
    )
    ordering = ['-reaction_count']


# Supabase Tables
admin.site.register(DiscordMessage, DiscordMessageAdmin)
admin.site.register(DiscordReaction, DiscordReactionAdmin)
admin.site.register(Log, LogAdmin)
admin.site.register(TopMessage, TopMessageAdmin)

# default database Tables
admin.site.register(GardenMessage, GardenMessageAdmin)
admin.site.register(Rule, RuleAdmin)
# admin.site.register(Author)
# admin.site.register(Reaction)
# admin.site.register(Message)
# admin.site.register(Score)
