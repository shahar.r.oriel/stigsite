from django.db import models
# from django.db.models.signals import post_save
from django.utils import timezone

############################################################################
# Supabase discord messages and logs


class DiscordChannel(models.Model):
    _DATABASE = 'supabase'

    id = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=256, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'discord_channel'


class DiscordGuild(models.Model):
    _DATABASE = 'supabase'

    id = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=256, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'discord_guild'


class DiscordMessage(models.Model):
    _DATABASE = 'supabase'

    id = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True)
    edited_at = models.DateTimeField(blank=True, null=True)
    content = models.CharField(max_length=2048, blank=True, null=True)
    author = models.JSONField(blank=True, null=True)
    reactions = models.JSONField(blank=True, null=True)
    saved_at = models.DateTimeField(blank=True, null=True)
    jump_url = models.URLField(max_length=500, null=True)

    class Meta:
        managed = False
        db_table = 'discord_message'


class DiscordReaction(models.Model):
    _DATABASE = 'supabase'

    id = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True)
    message_id = models.BigIntegerField(blank=True, null=True)
    user = models.BigIntegerField(blank=True, null=True)
    emoji = models.TextField(blank=True, null=True)
    previous_reactions = models.JSONField(blank=True, null=True)
    previous_reaction_users = models.JSONField(blank=True, null=True)
    users = models.JSONField(blank=True, null=True)
    # discord_message = models.ForeignKey(DiscordMessage, models.DO_NOTHING, db_column='discord_message', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'discord_reaction'

    def __str__(self):
        return str(self.emoji)


class DiscordUser(models.Model):
    """ A Discord user who successfully authenticated themselves through OAuth2 """

    _DATABASE = 'supabase'

    id = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=256, blank=True, null=True)
    display_name = models.CharField(max_length=256, blank=True, null=True)
    discriminator = models.CharField(max_length=256, blank=True, null=True)
    bot = models.BooleanField(blank=True, null=True)
    system = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'discord_user'


# class AuthUser(models.Model):
#     """ A Discord user who successfully authenticated themselves through OAuth2 """

#     _DATABASE = 'supabase'

#     id = models.BigAutoField(primary_key=True)
#     created_at = models.DateTimeField(blank=True, null=True)
#     name = models.CharField(max_length=256, blank=True, null=True)
#     display_name = models.CharField(max_length=256, blank=True, null=True)
#     discriminator = models.CharField(max_length=256, blank=True, null=True)
#     bot = models.BooleanField(blank=True, null=True)
#     system = models.BooleanField(blank=True, null=True)

#     class Meta:
#         managed = False
#         db_table = 'auth_user'


class MessageBase(models.Model):
    """ Contains the common fields between the TopMessage and GardenMessage tables

    This is not a database table itself
    """

    id = models.BigAutoField(primary_key=True)
    author = models.JSONField(blank=True, null=True)
    author_discord_username = models.CharField(
        null=True,
        blank=True,
        max_length=255,
        help_text="The Discord id of the message's author"
    )
    message_id = models.BigIntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    original_message = models.TextField(
        null=False,
        help_text="The message of the state written in English",
    )
    reactions = models.JSONField(
        null=False,
        help_text="A list of reaction data for a message"
    )
    reaction_count = models.FloatField(
        null=True,
        help_text="Denormalized snapshot of the number of reactions for a message when leaderboard record created.",
    )
    emoji_list = models.JSONField(
        null=True,
        help_text="A list of emojis in unicode strings format"
    )
    user_count = models.IntegerField(
        null=True,
        help_text="The number of users who reacted to the message"
    )
    user_list = models.JSONField(
        null=True,
        help_text="A list of users who reacted to the message"
    )
    unique_reaction_count = models.IntegerField(
        null=True,
        help_text="Denormalized snapshot of the number of reactions for a message when leaderboard record created.",
    )
    unique_emoji_list = models.JSONField(
        null=True,
        help_text="A list of emojis in unicode strings format"
    )
    unique_user_count = models.IntegerField(
        null=True,
        help_text="The number of users who reacted to the message"
    )
    unique_user_list = models.JSONField(
        null=True,
        help_text="A list of users who reacted to the message"
    )
    counts_by_emoji = models.JSONField(
        null=True,
        help_text="The number of users who reacted to the message"
    )
    exported_at = models.DateTimeField(blank=True, null=True)
    jump_url = models.URLField(max_length=500, null=True)

    class Meta:
        abstract = True


class TopMessage(MessageBase):
    """ A leaderboard table for messages the bot can send.
    Each row contains one message written in one language.
    """
    _DATABASE = 'supabase'

    added_to_garden = models.BooleanField(
        null=True,
        default=False,
        help_text="When toggled True, the message will create a GardenMessage record"
    )

    class Meta:
        managed = False
        db_table = 'top_message'


class GardenMessage(MessageBase):
    """ A collect of messages sent from the TopMessage table """
    _DATABASE = 'supabase'

    # id = models.BigAutoField(primary_key=True)

    user_id = models.CharField(
        null=True,
        blank=True,
        max_length=255,
        help_text="The id of the user who triggered the TopMessage.add_to_garden"
    )
    editable_message = models.TextField(
        null=False,
        help_text="The message of the state written in English",
    )
    edited_at = models.DateTimeField(blank=True, null=True)
    twitter_handle = models.CharField(
        null=True,
        blank=True,
        max_length=255,
        help_text="The message of the state written in English",
    )

    class Meta:
        managed = False
        db_table = 'garden_message'

    def save(self, *args, **kwargs):
        if self.original_message != self.editable_message:
            self.edited_at = timezone.now()
        super().save(*args, **kwargs)


class Log(models.Model):
    _DATABASE = 'supabase'

    id = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True)
    json = models.JSONField(blank=True, null=True)
    text = models.CharField(max_length=256, blank=True, null=True)
    log_level = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'log'


# Supabase discord messages and logs
############################################################################
