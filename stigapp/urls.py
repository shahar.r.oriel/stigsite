from django.urls import path

from .views import (
    DiscordMessageListView,
    DiscordReactionListView,
    GardenMessageListView,
    # LeaderBoardView,
    home,
    about,
    profile,
    export_top_message_csv,
    export_garden_message_csv,
    update_top_message_list
)

urlpatterns = [
    path('', home, name='home'),
    path('about', about, name='about'),
    path('profile', profile, name='profile'),
    path('garden', GardenMessageListView.as_view(), name='garden_messages'),
    # path('leaderboard', LeaderBoardView.as_view(), name='leaderboard-list'),
    path('leaderboard/messages', DiscordMessageListView.as_view(), name='message-list'),
    path('leaderboard/reactions', DiscordReactionListView.as_view(), name='reaction-list'),
    path('export-tm', export_top_message_csv, name='export_topmessage'),
    path('export-gm', export_garden_message_csv, name='export_gardenmessage'),
    path('update-tm', update_top_message_list, name='update_topmessage')
]
