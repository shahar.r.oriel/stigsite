# from `python manage.py inspectdb --database supabase`

from django.db import models


class DiscordChannel(models.Model):
    id = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=-1, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'discord_channel'


class DiscordGuild(models.Model):
    id = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=-1, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'discord_guild'


class DiscordMessage(models.Model):
    id = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True)
    edited_at = models.DateTimeField(blank=True, null=True)
    content = models.CharField(max_length=-1, blank=True, null=True)
    author = models.JSONField(blank=True, null=True)
    reactions = models.JSONField(blank=True, null=True)
    saved_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'discord_message'


class DiscordReaction(models.Model):
    id = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True)
    message_id = models.BigIntegerField(blank=True, null=True)
    user = models.BigIntegerField(blank=True, null=True)
    emoji = models.TextField(blank=True, null=True)
    previous_reactions = models.JSONField(blank=True, null=True)
    previous_reaction_users = models.JSONField(blank=True, null=True)
    users = models.JSONField(blank=True, null=True)
    discord_message = models.ForeignKey(DiscordMessage, models.DO_NOTHING, db_column='discord_message', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'discord_reaction'


class DiscordUser(models.Model):
    id = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=-1, blank=True, null=True)
    display_name = models.CharField(max_length=-1, blank=True, null=True)
    discriminator = models.CharField(max_length=-1, blank=True, null=True)
    bot = models.BooleanField(blank=True, null=True)
    system = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'discord_user'


class Log(models.Model):
    id = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True)
    json = models.JSONField(blank=True, null=True)
    text = models.CharField(max_length=-1, blank=True, null=True)
    log_level = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'log'
