from django.db import models
# from django.db.models.signals import post_save

from stigapp.models_discord_supabase import *  # noqa


##############################################
# TBD placeholders for local sqlite records (leaderboard, etc)

# class Author(models.Model):
#     _DATABASE = 'default'


# class Reaction(models.Model):
#     _DATABASE = 'default'


# class Message(models.Model):
#     """ A table for messages the bot can send.
#     Each row contains one message written in one language.
#     """
#     _DATABASE = 'default'

#     content = models.TextField(
#         null=False,
#         help_text="The message of the state written in English"
#     )

#
################################################################


class Rule(models.Model):
    ROLE_CHOICES = [
        ('admin-user', 'Admin User'),
        ('internal-user', 'Internal User'),
        ('gardner-user', 'Gardner User'),
        ('end-user', 'End User'),
    ]
    rule_name = models.CharField(
        null=True,
        max_length=255,
        help_text="The name of the rule"
    )
    chosen_reactions = models.JSONField(
        null=True,
        help_text="Reactions the rule allows"
    )
    channel_name = models.CharField(
        null=True,
        max_length=255,
        help_text="The channel name the rule applies to"
    )
    role = models.CharField(
        null=True,
        choices=ROLE_CHOICES,
        max_length=255,
        help_text="The role permissions for the rule"
    )
    threshold = models.FloatField(
        null=True,
        help_text="The threshhold the rule uses before acting"
    )
    activated_status = models.BooleanField(
        null=True,
        default=False,
        help_text="Whether the rule is active or disabled"
    )

# class Score(models.Model):
#     """ A table of supported user inputs that connect two states.

#     Users can enter the intent_text by button click or text input.

#     Each row is one trigger option written in one language.
#     """
#     # Foreign Keys
#     leader_board = models.ForeignKey(
#         LeaderBoard,
#         on_delete=models.CASCADE,
#         null=True,
#         blank=True,
#         help_text="Message reacted to."
#     )
#     reaction = models.ForeignKey(
#         Reaction,
#         on_delete=models.CASCADE,
#         null=True,
#         blank=True,
#         help_text="Emoji or sentiment."
#     )
#     votes = models.IntegerField(
#         null=False,
#         help_text="The number of 'votes' for this reaction."
#     )
#     weight = models.FloatField(
#         null=False,
#         max_length=255,
#         help_text="Importance or strength of this reaction."
#     )
