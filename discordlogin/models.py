import hashlib
import time
import datetime

from django.contrib.auth.models import (
    BaseUserManager,
    UserManager,
    AbstractBaseUser,
    AbstractUser,
)
from django.db import models
from django.utils import timezone


class UserManager(UserManager):

    def check_for_admin_authorization(self, discord_tag):
        """ Checks against a hardcoded list to see if a user is approved for admin privileges"""

        check = False
        approved_admin_users = [
            # 'insert discord_tags',
            'thompsgj#8407',
            'hobs#3386',
            'hobs#9238',
            'Sha#6179',
            'ronent#2267',
            'mariadyshel#6267',
        ]
        if discord_tag in approved_admin_users:
            check = True

        return check

    def create_password(self):
        """  Makes a string for the mandatory, non-nullable password field.

        The password will not work even if a user knows it because authentication happens through Discord.
        """
        hash = hashlib.sha1()
        hash.update(str(time.time()).encode('utf-8'))
        return hash.hexdigest()[:7]

    def create_new_discord_user(self, user, password=None):
        """ Creates a user in the DiscordUser table.

        Uses the 'DiscordAuthenticationBackend' for authentication, not the standard Django auth backend.
        """

        discord_tag = '%s#%s' % (user['username'], user['discriminator'])
        permissions_boolean = self.check_for_admin_authorization(discord_tag)

        new_user = self.model(
            email='test@test.com'
        )
        new_user.id = user['id']
        new_user.username = discord_tag
        new_user.discord_tag = discord_tag
        new_user.avatar = user['avatar']
        new_user.public_flags = user['public_flags']
        new_user.flags = user['flags']
        new_user.locale = user['locale']
        new_user.mfa_enabled = user['mfa_enabled']
        new_user.active = True
        new_user.staff = permissions_boolean
        new_user.admin = permissions_boolean

        password = self.create_password()
        new_user.set_password(password)
        new_user.save(using=self._db)
        return new_user


class User(AbstractUser):
    """ A user who has logged in to stigsite through Discord """

    _DATABASE = 'supabase'

    objects = UserManager()

    # Discord-specific user attributes
    id = models.BigIntegerField(primary_key=True)
    username = models.CharField(max_length=100, unique=True)
    discord_tag = models.CharField(max_length=100, unique=True)
    first_name = models.CharField(max_length=100, null=True)
    last_name = models.CharField(max_length=100, null=True)
    avatar = models.CharField(blank=True, null=True, max_length=100)
    public_flags = models.IntegerField()
    flags = models.IntegerField()
    locale = models.CharField(max_length=100)
    mfa_enabled = models.BooleanField()
    date_joined = models.DateTimeField(default=timezone.now)
    last_login = models.DateTimeField(default=timezone.now)

    # Django admin user attributes
    # authenticated = models.BooleanField(default=True)
    active = models.BooleanField(default=True)
    staff = models.BooleanField(default=True)
    admin = models.BooleanField(default=True)
    email = models.CharField(max_length=255, default="test@test.com")

    USERNAME_FIELD = 'discord_tag'
    REQUIRED_FIELDS = []

    class Meta:
        managed = False
        db_table = 'auth_user'
        # app_label = 'user_data'

    def __str__(self):
        return self.email

    def get_full_name(self):
        return self.discord_tag

    def get_short_name(self):
        return self.discord_tag

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_active(self):
        return self.active

    @property
    def is_staff(self):
        return self.staff

    @property
    def is_admin(self):
        return self.admin

    @property
    def is_superuser(self):
        return self.admin

    @property
    def is_authenticated(self):
        return True


# class DiscordUserManager(BaseUserManager):

#     def check_for_admin_authorization(self, discord_tag):
#         """ Checks against a hardcoded list to see if a user is approved for admin privileges"""

#         check = False
#         approved_admin_users = [
#             # 'insert discord_tags',
#             'thompsgj#8407'
#         ]
#         if discord_tag in approved_admin_users:
#             check = True

#         return check

#     def create_password(self):
#         """  Makes a string for the mandatory, non-nullable password field.

#         The password will not work even if a user knows it because authentication happens through Discord.
#         """
#         hash = hashlib.sha1()
#         hash.update(str(time.time()).encode('utf-8'))
#         return hash.hexdigest()[:7]

#     def create_new_discord_user(self, user, password=None):
#         """ Creates a user in the DiscordUser table.

#         Uses the 'DiscordAuthenticationBackend' for authentication, not the standard Django auth backend.
#         """

#         discord_tag = '%s#%s' % (user['username'], user['discriminator'])
#         permissions_boolean = self.check_for_admin_authorization(discord_tag)

#         new_user = self.model(
#             email='test@test.com'
#         )
#         new_user.id = user['id']
#         new_user.discord_tag = discord_tag
#         new_user.avatar = user['avatar']
#         new_user.public_flags = user['public_flags']
#         new_user.flags = user['flags']
#         new_user.locale = user['locale']
#         new_user.mfa_enabled = user['mfa_enabled']
#         new_user.active = True
#         new_user.staff = permissions_boolean
#         new_user.admin = permissions_boolean

#         password = self.create_password()
#         new_user.set_password(password)
#         new_user.save(using=self._db)
#         return new_user


# class DiscordUser(AbstractBaseUser):
#     """ A user who has logged in to stigsite through Discord """

#     _DATABASE = 'supabase'

#     objects = DiscordUserManager()

#     # Discord-specific user attributes
#     id = models.BigIntegerField(primary_key=True)
#     discord_tag = models.CharField(max_length=100, unique=True)
#     avatar = models.CharField(blank=True, null=True, max_length=100)
#     public_flags = models.IntegerField()
#     flags = models.IntegerField()
#     locale = models.CharField(max_length=100)
#     mfa_enabled = models.BooleanField()
#     last_login = models.DateTimeField(null=True)

#     # Django admin user attributes
#     # authenticated = models.BooleanField(default=True)
#     active = models.BooleanField(default=True)
#     staff = models.BooleanField(default=True)
#     admin = models.BooleanField(default=True)
#     email = models.CharField(max_length=255, default="test@test.com")

#     USERNAME_FIELD = 'discord_tag'
#     REQUIRED_FIELDS = []

#     class Meta:
#         managed = False
#         db_table = 'discord_user'

#     def __str__(self):
#         return self.email

#     def get_full_name(self):
#         return self.discord_tag

#     def get_short_name(self):
#         return self.discord_tag

#     def has_perm(self, perm, obj=None):
#         return True

#     def has_module_perms(self, app_label):
#         return True

#     @property
#     def is_active(self):
#         return self.active

#     @property
#     def is_staff(self):
#         return self.staff

#     @property
#     def is_admin(self):
#         return self.admin

#     @property
#     def is_authenticated(self):
#         return True
