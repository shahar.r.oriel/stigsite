from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import Group
# from .models import DiscordUser


from .forms import UserChangeForm, UserCreationForm
from .models import User


class UserAdmin(BaseUserAdmin):
    """ Controls the admin view of the custom User model """
    form = UserChangeForm
    add_form = UserCreationForm
    actions = ['delete_selected']

    list_display = ('discord_tag', 'is_admin')
    list_filter = ('admin',)

    fieldsets = (
        (None, {
            'fields': (
                'username',
                'discord_tag',
                'first_name',
                'last_name',
                'avatar',
                'public_flags',
                'flags',
                'locale',
                'active',
                'staff',
                'admin',
                'email',
                'password')
        }),
    )

    # Adds fields to the default user creation form
    add_fieldsets = (
        (None, {
            'fields': (
                'discord_tag',
                'id',
                'password1',
                'password2'
            )
        }),
    )

    def delete_selected(self, request, obj):
        """ TODO: Find a way to avoid manually making databases to support this

        Currently relies on auth_user and auth_user_user_permissions even though there isn't any data in them

        """
        for user in obj.all():
            User.objects.get(id=user.id).delete()



# Register your models here.
# admin.site.register(DiscordUser)
admin.site.unregister(Group)
admin.site.register(User, UserAdmin)
