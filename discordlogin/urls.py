from django.urls import path

from .views import (
    discord_login_redirect,
    get_authenticated_user
)

urlpatterns = [
    path('auth/user', get_authenticated_user, name="get_authenticated_user"),
    path('login/redirect', discord_login_redirect, name='discord_login_redirect')
]
