import os
import requests

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.db.models.query import QuerySet
from django.http import HttpRequest, HttpResponse, JsonResponse
from django.shortcuts import redirect
from django.urls import reverse

# Create your views here.


def home(request: HttpRequest) -> JsonResponse:
    return JsonResponse({'msg': 'Hello'})


@login_required(login_url="/login")
def get_authenticated_user(request: HttpRequest):
    user = request.user

    return JsonResponse({
        "id": user.id,
        "discord_tag": user.discord_tag,
        "avatar": user.avatar,
        "public_flags": user.public_flags,
        "flags": user.flags,
        "locale": user.locale,
        "mfa_enabled": user.mfa_enabled
    })


def discord_login(request: HttpRequest):
    return redirect(os.getenv('DISCORD_LOGIN_AUTH_URL'))


def discord_logout(request: HttpRequest):
    logout(request)
    return redirect(reverse('home'))


def discord_login_redirect(request: HttpRequest):
    """ Requests for a code that is exchanged for user's access tokens.

    Redirects user based on the status of their authentication
    """
    # Get code from query parameter
    print("1")
    code = request.GET.get('code')

    # Exchange the code with an access token
    user = exchange_code(code, request)

    print("2")
    # Handles when user cancels authentication from Discord
    if user is None:
        # return HttpResponse("You cancelled authentication")
        return redirect(reverse('home'))

    print("3")
    print(user)
    # Authenticate the user against the database
    discord_user = authenticate(request, user=user)

    print("4")
    # Handles if user tries to log in twice while already logged in
    if isinstance(discord_user, QuerySet):
        discord_user = list(discord_user).pop()

    # Specifies which backend Django should use to check and persist authentication
    print("5")
    login(request, discord_user, backend='discordlogin.auth.DiscordAuthenticationBackend')

    # return redirect(reverse(get_authenticated_user))
    return redirect(reverse('garden_messages'))


def exchange_code(code: str, request):
    """ Makes a request to the Discord token endpoint to get Access and Refresh Tokens

    access_token = allow making request on behalf of the user
    refresh_token = allows for refreshing the access_token


    If successful, redirects the user to the page for authenticated users

    """
    data = {
        "client_id": os.getenv('DISCORD_LOGIN_CLIENT_ID'),
        "client_secret": os.getenv('DISCORD_LOGIN_CLIENT_SECRET'),
        "grant_type": "authorization_code",
        "code": code,
        "redirect_uri": request.build_absolute_uri(reverse(discord_login_redirect)),
        "scope": "identify"
    }
    headers = {
        "Content-Type": "application/x-www-form-urlencoded"
    }
    response = requests.post("https://discord.com/api/oauth2/token", data=data, headers=headers)
    credentials = response.json()

    # Handles when user cancels authentication from Discord page
    if "error" in list(credentials.keys()):
        return None

    access_token = credentials['access_token']
    response = requests.get("https://discord.com/api/v6/users/@me", headers={
        'Authorization': 'Bearer %s' % access_token
    })

    user = response.json()
    return user
