# import os
from dotenv import load_dotenv, dotenv_values

# creates os environment variables from the .env file
load_dotenv()

CONFIG = dotenv_values()
globals().update(CONFIG)


PRINT_LOG_LEVEL = 10
BOTNAME = 'stigbot'
LOG_LEVEL = 10


DEFAULT_MESSAGE_ACTIONS = (
    # TODO: change these default args to None, and then allow the 3-tuple to be a 1-tuple and then just a str (without messing it up)
    ('record_message', (), {}),
    ('add_reaction', ('✔',), dict(condition=dict(regex=f'\\b{BOTNAME}\\b'))),
    # ('add_reaction', ('✍️',), {}),  # 🛢  💾  📓  📄  📒  🏷   ✔  ✅  ✴  ✳  🔹  ◼  ▫
    # ('reply', ['noted'], {}),
    # TODO allow format string templates with `def render_fstring(fstring, message):`
    # ('channel.send', ['noted'], {}),
)

DEFAULT_REACTION_ACTIONS = (
    ('record_reaction', (), {}),
    # ('add_reaction', ['✔'], {}),  # ✍️  🛢  💾  📓  📄  📒  🏷   ✔  ✅  ✴  ✳  🔹  ◼  ▫
    # ('reply', ['noted'], {}),
    # TODO allow format string templates with `def render_fstring(fstring, message):`
    # ('channel.send', ['noted'], {}),
)

# Python function names that user's can specify as actions when new messages, reactions, or users are received from discord
ALLOWED_FUNS = 'record_reaction record_message insert_supabase add_reaction reply channel.send'.split()
