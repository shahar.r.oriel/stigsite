"""
ASGI config for stigsite project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.1/howto/deployment/asgi/
"""

# Original
# import os

# from django.core.asgi import get_asgi_application

# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "stigsite.settings")

# application = get_asgi_application()


# Channels-discord
import os

from django.core.asgi import get_asgi_application
from channels.routing import ProtocolTypeRouter
from stigapp.consumers import MyDiscordConsumer


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "stigsite.settings")

django_application = get_asgi_application()


application = ProtocolTypeRouter({
    'http': django_application,
    'discord': MyDiscordConsumer
})
